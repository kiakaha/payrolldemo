class Timecard < ApplicationRecord
	belongs_to :employee
 
	def self.import(file)
	  spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(1)
	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    timecard = new
	    timecard.attributes = row.to_hash
	    timecard.save!
	  end
	end  

	# scope :monthly, lambda { |month| where('morning_in LIKE :month OR afternoon_out LIKE :month OR overtime_in LIKE :month OR overtime_out LIKE :month', :month => "%#{month}%") }
	scope :monthly, lambda { |from, to| where(:morning_in => from..to).or(Timecard.where(:overtime_in => from..to)) }
	scope :period, 	lambda { |from, to| where(:morning_in => from..to) }
	scope :overtime, 	lambda { |from, to| where(:overtime_in => from..to) }
end
