class Datasummary < ApplicationRecord
	belongs_to :employee

	def self.import(file)
	  spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(1)
	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    datasummary = new
	    datasummary.attributes = row.to_hash
	    datasummary.save!
	  end
	end  

	scope :paydate,	lambda { |paydate| where(:paydate => paydate) }
end
