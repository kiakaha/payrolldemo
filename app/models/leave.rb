class Leave < ApplicationRecord
	belongs_to :employee

	scope :period, lambda { |from,to,kind| where(:schedule => from..to).where(:kind=>kind) }
end
