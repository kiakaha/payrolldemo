class Tracking < ApplicationRecord
	belongs_to :employee

	def self.import(file)
	  spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(1)
	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    tracking = new
	    tracking.attributes = row.to_hash
	    tracking.save!
	  end
	end
end
