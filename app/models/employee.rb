class Employee < ApplicationRecord
	has_many :timecards, 			:foreign_key => :employee_no, :primary_key => :employee_no
	has_many :leaves, 				:foreign_key => :employee_no, :primary_key => :employee_no
	has_many :datasummaries, 	:foreign_key => :employee_no, :primary_key => :employee_no
	has_many :trackings,			:foreign_key => :employee_no, :primary_key => :employee_no
	# has_many :absences

	def self.import(file)
	  spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(2)
	  (3..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    employee = new
	    employee.attributes = row.to_hash
	    employee.save!
	  end
	end 
end
