class EmployeeMailer < ApplicationMailer
  helper :application
	default from: 'margotphils@gmail.com'

  def payslip(employee, pdf, paydate)
    @e = employee

    attachments["Payslip #{paydate} for #{@e.last_name}, #{@e.given_name}.pdf"] = pdf

    mail(to: @e.email,
         subject: "Margot Payslip #{paydate}") do |format|
      format.html { render 'payslip' }
      format.text { render text: 'Render text' }
    end
  end
end
