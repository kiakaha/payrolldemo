class PhilhealthsController < ApplicationController
  before_action :set_philhealth, only: [:update, :destroy]

  def create
    run Philhealth::Create do |result|
      return redirect_to settings_path
    end  
  end

  def update
    run Philhealth::Update do |result|
      return redirect_to settings_path
    end  
  end

  def destroy
    @philhealth.destroy
    respond_to do |format|
      format.html { redirect_to settings_path, notice: 'Philhealth was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_philhealth
      @philhealth = Philhealth.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def philhealth_params
      params.fetch(:philhealth, {})
    end
end
