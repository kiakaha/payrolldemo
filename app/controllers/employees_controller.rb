class EmployeesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_employee, only: [:update, :destroy]

  def index
  end

  def show
  end

  def create
    run Employee::Create do |result|
      return redirect_to employee_path(result['model'].id), notice: "Employee successfully created!"
    end 
  end

  def update
    run Employee::Update do |result|
      redirect_to employee_path(result['model'].id), notice: "Employee successfully updated!"
    end  
  end

  def destroy
    run Employee::Destroy do |op|
      redirect_to employees_path, notice: 'Employee was successfully deleted.'
    end  
  end

  def import
    Employee.import(params[:file])
    redirect_to employees_path
  end  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      # params.fetch(:employee, {})
      params.require(:employee).permit(:id, :first_name, :middle_name, :last_name)
    end

end
