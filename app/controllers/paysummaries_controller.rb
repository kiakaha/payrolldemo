class PaysummariesController < ApplicationController
  before_action :authenticate_user!
  def index
  end

  def report
    @employees = params[:branch] ? Employee.where(:branch=>params[:branch]).order(:last_name) : Employee.order(:last_name)
    @pay = {}
    @employees.each do |e| 
      if pay(e).present?
        basic       = e.pay_type=='Monthly' ? e.basic_salary.to_f / 2 : e.daily_rate.to_f * pay(e)[0][:total_days].to_f
        @pay[e.id]  = {
          :name             => e.last_name.to_s + ', ' + e.given_name.to_s + ' ' +e.middle_name.to_s,
          :basic            => '%.2f' % basic,
          :total_ot         => '%.2f' % (pay(e)[0][:reg_ot] + pay(e)[0][:rd_ot] + pay(e)[0][:sp_ot] + pay(e)[0][:hot] + pay(e)[0][:ext_ot]),
          :tardy            => '%.2f' % pay(e)[0][:tardy],
          :absences         => '%.2f' % pay(e)[0][:absences],
          :leave_conversion => '%.2f' % (pay(e)[0][:sl] + pay(e)[0][:vl]),
          :night_diff       => '%.2f' % pay(e)[0][:night_diff],
          :gov_inc_loans    => '%.2f' % (pay(e)[0][:philhealth_15th] + pay(e)[0][:hdmf_15th] + pay(e)[0][:hdmf_ln_15th] + pay(e)[0][:sss_30th] + pay(e)[0][:sss_ln_30th] + pay(e)[0][:insurance] + pay(e)[0][:margot_ca]), 
          :net_tax          => '%.2f' % pay(e)[2][:net_tax]
        }
      end  
    end  
    
    respond_to do |format|
      format.html
      format.xlsx { response.headers['Content-Disposition'] = 'attachment; filename="Pay Summary '+ params[:paydate] + '.xlsx"' }
    end
  end

  def bankreport
    @employees = Employee.order(:last_name).where.not(:bankaccount_no=>nil)
    @net = {}
    @acc = {}
    @employees.each do |e| 
      if pay(e).present?
        @net[e.id] = ('%.2f' % (pay(e)[1][:other_pays_total] + pay(e)[1][:earnings_total] - pay(e)[3][:gov_deduct_total] - pay(e)[1][:ln_others_total])) 
        @acc[e.id] = e.bankaccount_no.gsub('-', '')
      end  
    end  
    respond_to do |format|
      format.html
      format.xlsx { response.headers['Content-Disposition'] = 'attachment; filename="Bank Report '+ params[:paydate] + '.xlsx"' }
    end
  end


  private

  def pay(e) 
    s   = e.datasummaries.where(:paydate => (params[:paydate]).to_datetime).first 
    if s.present? 
      pay = { 
        :id               => s.id,  
        :total_days       => s.total_days.to_f,  
        :reg_ot_time      => s.reg_ot, 
        :reg_ot           => s.reg_ot.to_f          * e.hourly_rate.to_f * 1.25, 
        :rd_ot_time       => s.rd_ot,
        :rd_ot            => s.rd_ot.to_f           * e.hourly_rate.to_f * 1.3,
        :sp_ot_time       => s.sp_ot, 
        :sp_ot            => s.sp_ot.to_f           * e.hourly_rate.to_f * 1.3, 
        :hot_time         => s.hot, 
        :hot              => s.hot.to_f             * e.hourly_rate.to_f * 2, 
        :ext_ot_time      => s.ext_ot,
        :ext_ot           => s.ext_ot.to_f          * e.hourly_rate.to_f * 0.3,
        :tardy            => s.tardy.to_f           * (e.hourly_rate.to_f / 60),
        :absences         => s.absences.to_f        * e.daily_rate.to_f,
        :vl_count         => s.vl,
        :vl               => s.vl.to_f              * e.daily_rate.to_f,
        :sl_count         => s.sl,
        :sl               => s.sl.to_f              * e.daily_rate.to_f,
        :night_diff       => s.night_diff.to_f      * e.hourly_rate.to_f * 0.1,
        :ot_meals         => s.ot_meals.to_f        * e.ot_meal.to_f,

        :philhealth_15th  => s.philhealth_15th.to_f * e.government_philhealth.to_f,
        :hdmf_15th        => s.hdmf_15th.to_f       * e.government_hdmf.to_f,
        :hdmf_ln_15th     => s.hdmf_ln_15th.to_f    * e.loan_hdmf.to_f,
        :sss_30th         => s.sss_30th.to_f        * e.government_sss.to_f,
        :sss_ln_30th      => s.sss_30th.to_f        * e.loan_sss.to_f,
        :insurance        => s.insurance.to_f       * e.loan_insurance.to_f,

        :margot_ca        => s.margot_ca.to_f       * e.loan_margot.to_f,
        :adjustments      => s.adjustments.to_f,
        :pay_13th         => s.pay_13th.to_f,
        :refund           => 0,
        :tax_credit       => 0,
        :other_deductions => 0
      } 
      basic               = e.pay_type=='Monthly'||e.pay_type.nil? ? e.basic_salary.to_f / 2 : e.daily_rate.to_f * s.total_days.to_f
      total               = {
        :first_total      => pay[:reg_ot] + pay[:rd_ot] + pay[:sp_ot] + pay[:hot] + pay[:ext_ot] - pay[:tardy] - pay[:absences] + pay[:vl] + pay[:sl] + pay[:night_diff],
        :second_total     => pay[:philhealth_15th] + pay[:hdmf_15th] + pay[:sss_30th],
        :ot_total         => pay[:reg_ot] + pay[:rd_ot] + pay[:hot] + pay[:sp_ot] + pay[:ext_ot],
        :earnings_total   => basic - (pay[:tardy] + pay[:absences]) + pay[:night_diff] + pay[:pay_13th] + (pay[:reg_ot] + pay[:rd_ot] + pay[:hot] + pay[:sp_ot] + pay[:ext_ot]),
        :other_pays_total => (e.monthly.to_f / 2) + pay[:ot_meals] + pay[:vl] + pay[:sl] + (e.cola.to_f / 2) + pay[:refund] + pay[:tax_credit],
        :ln_others_total  => pay[:sss_ln_30th] + pay[:hdmf_ln_15th] + pay[:margot_ca] + pay[:other_deductions], 
      }

      ot_amount           = total[:first_total] - total[:second_total]
      ot_tax              = ot_amount * e.tax_rate.to_f
      tax                 = {
        :ot_amount        => ot_amount,
        :ot_tax           => ot_tax,
        :net_tax          => (e.tax_gross.to_f / 2) + ot_tax  
      }

      paysummary          = (e.basic_salary.to_f/2) + total[:first_total] - total[:second_total] - pay[:margot_ca] - pay[:adjustments] + pay[:pay_13th] + pay[:refund] - tax[:net_tax] + ((e.monthly.to_f + e.cola.to_f) / 2) + pay[:ot_meals] - pay[:sss_ln_30th] - pay[:hdmf_ln_15th]
      payslip             = 0
      proofing            = {
        :paysummary       => paysummary,
        :payslip          => payslip,
        :diff             => paysummary - payslip,
        :gov_deduct_total => pay[:sss_30th] + pay[:philhealth_15th] + pay[:hdmf_15th] + tax[:net_tax] + pay[:adjustments] 
      }

      [] << pay << total << tax << proofing 
    end  
  end  

end
