class DeductionsController < ApplicationController
  before_action :set_deduction, only: [:update, :destroy]
  
  def create
    run Deduction::Create do |result|
      return redirect_to settings_path
    end  
  end

  def update
    run Deduction::Update do |result|
      return redirect_to settings_path
    end
  end

  def destroy
    @deduction.destroy
    respond_to do |format|
      format.html { redirect_to settings_path, notice: 'Deduction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deduction
      @deduction = Deduction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deduction_params
      params.fetch(:deduction, {})
    end
end
