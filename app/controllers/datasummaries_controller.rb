class DatasummariesController < ApplicationController
  before_action :authenticate_user!
  def index
  end

  def update
  	run Datasummary::Update do |result|
  		return redirect_to :back
  	end	
  end	

  def import
    Datasummary.import(params[:file])
    redirect_to paysummaries_path
  end 

  def remove
    data = Datasummary.where(:paydate => (params[:paydate]).to_datetime).where(:period_from => (params[:from]).to_datetime).where(:period_to => (params[:to]).to_datetime).delete_all
    respond_to do |format|
      format.html { redirect_to paysummaries_path, notice: 'Paydate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end 
  
end
