class StatusesController < ApplicationController
  before_action :set_status, only: [:update, :destroy]

  def create
    run Status::Create do |result|
      return redirect_to settings_path
    end  
  end

  def update
    run Status::Update do |result|
      return redirect_to settings_path
    end  
  end

  def destroy
    @status.destroy
    respond_to do |format|
      format.html { redirect_to settings_path, notice: 'Status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_status
      @status = Status.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def status_params
      params.fetch(:status, {})
    end
end
