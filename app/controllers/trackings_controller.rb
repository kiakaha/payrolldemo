class TrackingsController < ApplicationController
  before_action :authenticate_user! 
  
  def update
    run Tracking::Update do |result|
      return redirect_to trackings_path
    end 
  end

  def import
    Tracking.import(params[:file])
    redirect_to :back
  end 
end
