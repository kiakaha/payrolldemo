class LeavetypesController < ApplicationController
  before_action :set_leavetype, only: [:update, :destroy]

  def create
    run Leavetype::Create do |result|
      return redirect_to settings_path
    end  
  end

  def update
    run Leavetype::Update do |result|
      return redirect_to settings_path
    end  
  end

  def destroy
    @leavetype.destroy
    respond_to do |format|
      format.html { redirect_to settings_path, notice: 'Leavetype was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_leavetype
      @leavetype = Leavetype.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def leavetype_params
      params.fetch(:leavetype, {})
    end
end
