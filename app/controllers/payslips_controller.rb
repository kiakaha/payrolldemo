class PayslipsController < ApplicationController

	layout 'printable'

  def index
  end

  def email
    paydate = params[:paydate]
    employees = params[:branch] ? Employee.where.not(email: nil).where(:branch=>params[:branch]) : params[:employee] ? Employee.where(:id=>params[:employee]) : Employee.where.not(email: nil)

    employees.each do |e|
      if e.email.present?
        kit = PDFKit.new(payslips_url(:employee=>e.id, :paydate=>paydate, :from=>params[:from], :to=>params[:to]))
        EmployeeMailer.payslip(e, kit.to_pdf, paydate).deliver_now
      end
    end

    redirect_to :back
  end
end
