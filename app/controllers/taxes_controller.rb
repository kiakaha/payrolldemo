class TaxesController < ApplicationController
  before_action :set_tax, only: [:update, :destroy]

  def create
    run Tax::Create do |result|
      return redirect_to settings_path
    end  
  end

  def update
    run Tax::Update do |result|
      return redirect_to settings_path
    end
  end

  def destroy
    @tax.destroy
    respond_to do |format|
      format.html { redirect_to settings_path, notice: 'Tax was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tax
      @tax = Tax.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tax_params
      params.fetch(:tax, {})
    end
end
