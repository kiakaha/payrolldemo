class PayrolltypesController < ApplicationController
  before_action :set_payrolltype, only: [:update, :destroy]

  def create
    run Payrolltype::Create do |result|
      return redirect_to settings_path
    end  
  end

  def update
    run Payrolltype::Update do |result|
      return redirect_to settings_path
    end  
  end

  def destroy
    @payrolltype.destroy
    respond_to do |format|
      format.html { redirect_to payrolltypes_url, notice: 'Payrolltype was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payrolltype
      @payrolltype = Payrolltype.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payrolltype_params
      params.fetch(:payrolltype, {})
    end
end
