class Api::V1::EmployeesController < Api::V1::BaseController 
	include Roar::Rails::ControllerAdditions
	represents :json, :entity => EmployeeRepresenter, :collection => EmployeeRepresenter.for_collection

	def index 
		employees = Employee.where(:branch=>params[:branch])
		respond_with employees
	end 
end