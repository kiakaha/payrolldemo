class TimecardsController < ApplicationController
  # before_action :timecards, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_employee, only: [:logs]

  def index
  end

  def import
    Timecard.import(params[:file])
    redirect_to :back
  end  

  def logs
    @tc = []
    timecards.each do |t|
      @tc << { :title => "Login #{t.morning_in.strftime('%I:%M %P')}",      :start => t.morning_in.to_date, :backgroundColor => '#0073b7', :borderColor => '#0073b7'} if t.morning_in
      @tc << { :title => "Logout #{t.afternoon_out.strftime('%I:%M %P')}",  :start => t.morning_in.to_date, :backgroundColor => '#00c0ef', :borderColor => '#00c0ef'} if t.afternoon_out
      @tc << { :title => "OT Login #{t.overtime_in.strftime('%I:%M %P')}",  :start => (t.morning_in || t.overtime_in).to_date, :backgroundColor => '#00a65a', :borderColor => '#00a65a'} if t.overtime_in
      @tc << { :title => "OT Logout #{t.overtime_out.strftime('%I:%M %P')}",:start => (t.morning_in || t.overtime_out).to_date, :backgroundColor => '#f39c12', :borderColor => '#f39c12'} if t.overtime_out
    end

    @employee.leaves.each do |l| @tc << { :title => "#{l.kind} - #{l.remarks}", :start => l.schedule.to_date, :backgroundColor => '#f56954', :borderColor => '#f56954'} if l.schedule end

    rds          = (from..to).group_by(&:wday)[@employee.shift_pattern_rd.to_i]
    rds.each do |rd| @tc << { :title => "Rest Day", :start => rd.to_date, :backgroundColor => '#f56954', :borderColor => '#f56954'} end  

    respond_to do |format| format.json { render :json => @tc } end  
  end

  def summary
    tardy   = timecards.where.not(:morning_in=>nil).map(&:morning_in).collect { |t| t.strftime("%H:%M:%S").to_time - (@employee.shift_pattern_regdays_in ? @employee.shift_pattern_regdays_in : '8 am').to_time }.select { |sec| sec.to_i > 0 }.inject(:+)
    reg_ot  = timecards.where.not(:overtime_in=>nil).map { |t| t.overtime_out.to_time - t.overtime_in.to_time }

    dates        = from..to
    rds          = dates.group_by(&:wday)[@employee.shift_pattern_rd.to_i]

    summary      = { 
        :total_days => timecards.count,  
        :reg_ot     => sec_to_ftime(reg_ot.inject(:+)), 
        :rd_ot      => @employee.shift_pattern_rd, 
        :sp_ot      => '', 
        :h_ot       => '', 
        :ext_ot     => '', 
        :tardy      => sec_to_ftime(tardy), 
        :absences   => dates.count - rds.count - timecards.count - @employee.leaves.count, 
        :vl         => @employee.leaves.where("kind='VL'").where(:schedule=>from..to).count, # days
        :sl         => @employee.leaves.where("kind='SL'").where(:schedule=>from..to).count, # days 
        :night_diff => timecards.count * (@employee.night_diff).to_f, # hours 
        :ot_meals   => reg_ot.count * (@employee.ot_meal).to_f  # of OT
      }

    respond_to do |format|
      format.json { render :json => summary }
    end  
  end  
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def timecards
      employee = set_employee
      @timecards = employee.timecards.monthly(from, to)
      # @timecards = employee.timecards.monthly('2017-03')
    end

    def from
      "#{params[:month]}-1".to_datetime
    end  

    def to
      to = "#{params[:month]}-1".to_datetime.end_of_month
      now = Time.now.to_datetime
      to > now ? now : to
    end  

    def set_employee
      @employee = Employee.find(params[:employee_id])
    end  

    # Never trust parameters from the scary internet, only allow the white list through.
    def timecard_params
      params.fetch(:timecard, {})
    end

    def sec_to_ftime(sec)
      if sec.present? 
        (sec / 3600).to_i.to_s.rjust(2, '0') + ':' + ((sec % 3600) / 60).to_i.to_s.rjust(2, '0') + ':' + ((sec % 3600) % 60).to_i.to_s.rjust(2, '0')
      else
        '00:00:00'
      end  
    end  
end
