// app/assets/javascripts/angular/controllers/users_controllers.js
var myApp = angular.module('myapplication', ['ngRoute', 'ngResource']);

myApp.factory('Employee', ['$resource',function($resource){
 return $resource('/employees.json', {},{
 query: { method: 'GET', isArray: true },
 create: { method: 'POST' }
 })
}]);
 
myApp.factory('Employee', ['$resource', function($resource){
 return $resource('/employees/:id.json', {}, {
 show: { method: 'GET' },
 update: { method: 'PUT', params: {id: '@id'} },
 delete: { method: 'DELETE', params: {id: '@id'} }
 });
}]);

myApp.config([
 '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
 $routeProvider.when('/employees',{
    // templateUrl: '/employees',
    controller: 'EmployeeListCtr'
 });
 $routeProvider.when('/employees/new', {
   // templateUrl: '/employees/new',
   controller: 'EmployeeAddCtr'
 });
 $routeProvider.when('/employees/:id/edit', {
   // templateUrl: '/templates/users/edit.html',
   controller: "EmployeeUpdateCtr"
 });
 $routeProvider.otherwise({
   redirectTo: '/employees'
 });
 }
]);

// Index Action
myApp.controller("EmployeeListCtr", ['$scope', '$resource', 'Employees', 'Employee', '$location', 
	function($scope, $resource, Employees, Employee, $location) {
  	$scope.employees = Employees.query(); //it's getting user collection
  	$scope.sample = "trest";

  	$scope.deleteEmployee = function (employeeId) {
	    if (confirm("Are you sure you want to delete this user?")){
	      Employee.delete({ id: employeeId }, function(){
	        $scope.employee = Employees.query();   // after delete user get users collection.
	        $location.path('/');
	      });
	    }
	  };

	}
]);

// Create Action:
myApp.controller("EmployeeAddCtr", ['$scope', '$resource', 'Employees', '$location', function($scope, $resource, Employees, $location) {
  $scope.save = function () {
    if ($scope.employeeForm.$valid){
      Employees.create({employee: $scope.employee}, function(){
      $location.path('/');
    }, function(error){
      console.log(error)
    });
  }
 }
}]);

// Update Action:
myApp.controller("EmployeeUpdateCtr", ['$scope', '$resource', 'Employee', '$location', '$routeParams', function($scope, $resource, Employee, $location, $routeParams) {
   $scope.employee = Employee.get({id: $routeParams.id})
   $scope.update = function(){
     if ($scope.employeeForm.$valid){
       Employee.update($scope.employee,function(){
         $location.path('/');
       }, function(error) {
         console.log(error)
      });
     }
   };
}]);
