var Paysummaries = React.createClass({ 
  getInitialState() { 
    return { employees: [] } 
  }, 

  componentWillMount() {
    $.getJSON('/api/v1/employees.json?branch='+this.props.branch, (response) => { this.setState({ employees: response }) }); 
  },
  
  componentDidMount() { 
    
  },

  componentDidUpdate: function(){
    $("#" + this.props.tableId).DataTable();
  },  

  render() { 
    var list = this.state.employees.map((employee) => { 
          return ( 
            <tr>
              <td>{employee.last_name}</td>
              <td>{employee.given_name}</td>
              <td>{employee.middle_name}</td>
              <td>{employee.job_no}</td>
              <td>{employee.employee_no}</td>
              <td>{employee.id}</td>
              <td>
                <div className="btn-group" role="group"> 
                  <button className="btn btn-default btn-flat btn-xs dropdown-toggle" aria-expanded="true" aria-haspopup="false" data-toggle="dropdown" type="button"> 
                    <i className="fa fa-ellipsis-h"></i>
                  </button>  
                  <ul className="dropdown-menu dropdown-menu-right">
                    <li>
                      <a href={"employees/" + employee.id}>
                        <i className="fa fa-user-circle-o"></i> Account
                      </a>
                    </li> 
                    <li>
                      <a href={"employees/" + employee.id + "/timecards"}>
                        <i className="fa fa-clock-o"></i> Timecard
                      </a>  
                    </li>  
                  </ul>
                </div>     
              </td>

            </tr>  
          ) 
        });

    return ( 
      <div className="table-responsive">
        <table className="table table-bordered table-striped" id={this.props.tableId}>
          <thead>
            <tr>
              <th> Last Name</th> 
              <th> Given Name</th> 
              <th> MI</th> 
              <th> Job No.</th> 
              <th> Employee No.</th> 
              <th> Sys ID</th> 
              <th></th>
            </tr>   
          </thead>    
          <tbody>
            {list}
          </tbody>
        </table> 
      </div> 
    ) 
  } 
});