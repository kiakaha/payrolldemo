module EmployeeRepresenter
  include Roar::JSON

  property :id
  property :given_name
  property :middle_name
  property :last_name
  property :email
  property :job_no
  property :employee_no
  property :employment_status
  
end












# require 'roar/decorator'
# require 'roar/json'

# class EmployeeRepresenter < Roar::Decorator
#   include Roar::JSON
#   include Roar::Hypermedia

#   property :id
#   property :given_name
#   property :middle_name
#   property :last_name
#   property :job_no
#   property :employee_no

#   # link :self do
#   #   "http://songs/#{represented.id}/#{@base_url}"
#   # end

#   collection :datasummaries, extend: DataRepresenter, class: Datasummary
# end