class Employee::Destroy < Trailblazer::Operation
	step :delete_employee!

  def delete_employee!(options, params:, **)
  	return invalid! unless params[:id]
    Employee.find_by(id: params[:id]).destroy
  end
	
end	