class Employee::Update < Trailblazer::Operation
	step Model( Employee, :find )
	step Contract::Build( constant: Employee::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end	