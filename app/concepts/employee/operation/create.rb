class Employee::Create < Trailblazer::Operation
	step Model( Employee, :new)
	step Contract::Build( constant: Employee::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	