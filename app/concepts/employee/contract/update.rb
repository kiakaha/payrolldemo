module Employee::Contract 
	class Update < Reform::Form
		# PERSONAL INFORMATION ================================
		property :branch
		property :job_no
		property :employee_no
		property :given_name
		property :middle_name
		property :last_name
		property :email
		property :address
		property :contact_no
		property :birthday
		property :civil_status
		property :height
		property :weight
		property :religion
		property :education
		property :skills
		property :motto
		property :designation
		property :team
		property :employment_status
		property :photo
		property :date_hired
		# =====================================================

		# BASIC PAY ===========================================
		property :basic_salary
		property :daily_rate # basic_salary / 26
		property :hourly_rate # daily_rate / 8
		property :night_diff 

		property :rata
		property :pera
		property :addl_comp
		
		property :bill_rate_per_day
		property :pay_type
		property :shift_pattern_regdays_in
		property :shift_pattern_regdays_out
		property :shift_pattern_weekdays_in
		property :shift_pattern_weekdays_out
		property :shift_pattern_rd
		# =====================================================

		# OTHER ALLOWANCE/BONUS ===============================
		property :monthly
		property :cola
		property :ot_meal

		property :pib
		property :clothing
		property :midyear
		property :loyalty
		property :year_end
		property :other_1
		property :other_2
		property :other_3
		property :dependent_1	
		property :dependent_2
		property :dependent_3
		property :dependent_4
		property :dependent_5
		# =====================================================

		# GOVERNMENT ==========================================
		property :government_sss
		property :government_hdmf
		property :government_philhealth
		property :government_insurance
		# =====================================================

		# TAXATION ============================================
		property :tax_exemption 
		property :tax_bracket
		property :tax_delta
		property :tax_rate
		property :tax_add_on
		property :tax_constant
		property :tax_gross
		# delta = basic_salary - tax_not_over
		# rate = tax_excess_percent
		# add_on = delta * rate
		# constant = tax_due
		# gross_tax = add-on + constant
				# --------- FROM PAY SUMMARY ---------------------------------------------
				# additional = REG_OT + RDOT + SP_OT + EXT_OT - TARDY - ABSENCES + VL + SL 
				# deduction  = PHILHEALTH(15th) + HDMF(15th) + SSS(30th)
		# ot_amount = addition - deduction
		# ot_tax = ot_amount * rate
		# net_tax = gross_tax + ot_tax
		# ================================================================

		# LOANS ==========================================================
		property :loan_sss
		property :loan_hdmf
		property :loan_margot
		property :loan_insurance

		property :loan_amount
		property :date_granted
		property :loan_interest
		property :loan_payments
		property :loan_balance

		# ================================================================

		# IMPORTANT NUMBERS ==============================================
		property :bankaccount_no
		property :sss_no
		property :philhealth_no
		property :hdmf_no
		property :tin_no
		property :hmo_no

		property :gsis_no
		property :atm_no
		property :bank_name
		property :bank_branch
		# ================================================================

		# @remove t.string :no_of_children			

		# TERMINATION ====================================================
		property :date_terminated
		property :reason
		# ================================================================

		validates :job_no, :employee_no, :given_name, :last_name, :branch, presence: true
	end
end			