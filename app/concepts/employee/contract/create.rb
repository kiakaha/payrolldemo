module Employee::Contract
	class Create < Reform::Form
		property :job_no
		property :employee_no
		property :given_name
		property :middle_name
		property :last_name
		property :branch

		validates :job_no, :employee_no, :given_name, :last_name, :branch, presence: true
	end
end