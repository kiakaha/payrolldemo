module Employee::Cell
	class Index < Trailblazer::Cell
		include ::React::Rails::ViewHelper
		
		property :leaves
		property :trackings

		private

		def new_employee
			Employee.new
		end	

		def branches
			Employee.select(:branch).group(:branch).order(:branch)
		end	

		def active(branch)
			first = branches.first
			branch==first.branch ? 'active' : ''
		end	
		
	end
end