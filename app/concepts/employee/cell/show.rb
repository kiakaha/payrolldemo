module Employee::Cell
	class Show < Trailblazer::Cell

		private
		
		def employee
			Employee.find(params[:id])
		end	

		def name
			employee.given_name + ' ' + employee.last_name
		end	
			
	end	
end		