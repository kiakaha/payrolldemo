module Deduction::Contract
	class Update < Reform::Form
		property :code
		property :description
		property :kind
		property :remark

		validates :code, :description, :kind, :remark, presence: true   
	end
end		