class Deduction::Create < Trailblazer::Operation
	step Model( Deduction, :new )
	step Contract::Build( constant: Deduction::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	