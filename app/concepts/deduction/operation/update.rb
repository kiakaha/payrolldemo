class Deduction::Update < Trailblazer::Operation
	step Model( Deduction, :find )
	step Contract::Build( constant: Deduction::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end	