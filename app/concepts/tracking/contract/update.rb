module Tracking::Contract
	class Update < Reform::Form
		property :year
		property :sl
		property :vl
		property :ml
		property :pl
		property :bl
		property :el

		validates :year, :sl, :vl, :ml, :pl, :bl, :el, presence: true
	end
end		