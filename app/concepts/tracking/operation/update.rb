class Tracking::Update < Trailblazer::Operation
	step Model( Tracking, :find )
	step Contract::Build( constant: Tracking::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end		