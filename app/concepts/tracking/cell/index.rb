module Tracking::Cell
	class Index < Trailblazer::Cell

		private

		def employees
			Employee.all
		end

		def branches
      Employee.select(:branch).group(:branch).order(:branch)
    end 

    def active(branch)
      first = branches.first
      branch==first.branch ? 'active' : ''
    end

		def leaves(employee)
			leaves = employee.datasummaries.where("sl!='' OR vl!='' OR ml!='' OR pl!='' OR el!='' OR bl!=''").where(:paydate=>"Jan, 1 #{year}".to_date.."Dec, 30 #{year}".to_datetime.end_of_month.to_date).order(:paydate)
			leaves.map { |l| {
					:paydate => l.paydate.to_date,
					:sl => l.sl||0,
					:vl => l.vl||0,
					:ml => l.ml||0,
					:pl => l.pl||0,
					:bl => l.bl||0,
					:el => l.el||0,
				}
			} 
		end

		def year
			params[:year] || Time.current.year
		end	

		def check_tracking
			if Tracking.where(:year=>year).count > 0
				true
			else 
				false
			end	
		end

		def tracking
			Tracking.new
		end

		def create_tracking(employee)
			track = employee.trackings.where(:year=>Time.current.year)
			if track.present?
				track.last
			else
				employee.trackings.create([:year=>Time.current.year, :sl=>0, :vl=>0, :ml=>0, :pl=>0, :bl=>0, :el=>0])
			end	
		end	

		def new_leave
			Leave.new
		end	

	end	
end		