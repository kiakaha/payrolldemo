class Leavetype::Create < Trailblazer::Operation
	step Model( Leavetype, :new )
	step Contract::Build( constant: Leavetype::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	