class Leavetype::Update < Trailblazer::Operation
	step Model( Leavetype, :find )
	step Contract::Build( constant: Leavetype::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end	