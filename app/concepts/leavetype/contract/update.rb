module Leavetype::Contract
	class Update < Reform::Form
		property :code
		property :description
		property :kind

		validates :code, :description, :kind, presence: true   
	end
end		