module Datasummary::Cell
	class Index < Trailblazer::Cell

		private

		def employees
			Employee.all
		end

		def branches
			Employee.select(:branch).group(:branch).order(:branch)
		end	

		def active(branch)
			first = branches.first
			branch==first.branch ? 'active' : ''
		end	

		def datasummaries
			Datasummary.select(:paydate,:period_from,:period_to).group(:paydate,:period_from,:period_to).order('paydate DESC')
		end	

		def summary(e) 
			if params[:data]=='saved' 
				s = e.datasummaries.where(:paydate => (params[:paydate]).to_datetime).last
		    if s.present?
			    { 
		        :id 							=> s.id,  
		        :total_days 			=> s.total_days.to_f,  
		        :reg_ot     			=> s.reg_ot.to_f, 
		        :rd_ot      			=> s.rd_ot.to_f,
		        :sp_ot      			=> s.sp_ot.to_f, 
		        :hot        			=> s.hot.to_f, 
		        :ext_ot     			=> s.ext_ot.to_f,
		        :tardy      			=> s.tardy.to_f,
		        :absences   			=> s.absences.to_f,
		        :vl         			=> s.vl.to_f,
		        :sl         			=> s.sl.to_f,
		        :leaves     			=> s.vl.to_f + s.sl.to_f,
		        :night_diff 			=> s.night_diff.to_f,
		        :ot_meals   			=> s.ot_meals.to_f,

		        :philhealth_15th 	=> s.philhealth_15th.to_f,
		        :hdmf_15th   			=> s.hdmf_15th.to_f,
		        :hdmf_ln_15th   	=> s.hdmf_ln_15th.to_f,
		        :sss_30th   			=> s.sss_30th.to_f,
		        :sss_ln_30th   		=> s.sss_ln_30th.to_f,
		        :insurance   			=> s.insurance.to_f,
		        :margot_ca   			=> s.margot_ca.to_f,
		        :adjustments   		=> s.adjustments.to_f,
		        :pay_13th   			=> s.pay_13th.to_f,
		        :total   					=> s.total.to_f
			    } 
			  end  
	    else
	    	from 								= period_from
				to									= period_to
				timecards 					= e.timecards.period(from,to)
				overtime 						= e.timecards.overtime(from,to) || {}
				rds									= (from..to).group_by(&:wday)[e.shift_pattern_rd.to_i] 
		    tardy   						= timecards.where.not(:morning_in=>nil).map(&:morning_in).collect { |t| t.strftime("%H:%M:%S").to_time - (e.shift_pattern_regdays_in!='' ? e.shift_pattern_regdays_in : '8 am' ).to_time }.select { |sec| sec.to_i > 0 }.inject(:+) if e.shift_pattern_regdays_in
		    reg_ot							= overtime.map { |t| t.overtime_out.to_time - t.overtime_in.to_time }.inject(:+)
		    rd_ot								= rds.present? ? (rds & overtime.map{ |t| t.overtime_in.to_date }).count : 0
		    sl 									= e.leaves.period(from,to,'SL').count
		    vl 									= e.leaves.period(from,to,'VL').count
		    
		    { 
	        :id 							=> ''.to_f,  
	        :total_days 			=> timecards.count.to_f,  
	        :reg_ot     			=> sec_to_ftime(reg_ot), 
	        :rd_ot      			=> rd_ot.to_f, 
	        :sp_ot      			=> ''.to_f, 
	        :hot       				=> ''.to_f, 
	        :ext_ot     			=> ''.to_f, 
	        :tardy      			=> sec_to_ftime(tardy), 
	        :absences   			=> (from..to).count - (rds.present? ? rds.count : 0) - timecards.count - e.leaves.period(from,to,'SL').count - e.leaves.period(from,to,'VL').count, 
	        :vl         			=> vl.to_f, # days
	        :sl         			=> sl.to_f, # days 
	        :leaves     			=> sl.to_f + vl.to_f, # days 
	        :night_diff 			=> timecards.count.to_f, # hours * (e.night_diff).to_f
	        :ot_meals   			=> overtime.count.to_f + rd_ot.to_f,  # no of OT

	        :philhealth_15th 	=> ''.to_f,
	        :hdmf_15th   			=> ''.to_f,
	        :hdmf_ln_15th   	=> ''.to_f,
	        :sss_30th   			=> ''.to_f,
	        :sss_ln_30th   		=> ''.to_f,
	        :insurance   			=> ''.to_f,
	        :margot_ca   			=> ''.to_f,
	        :adjustments   		=> ''.to_f,
	        :pay_13th   			=> ''.to_f,
	        :total   					=> ''.to_f
	      } 
	    end  
	  end 


	  def sec_to_ftime(sec)
      (sec / 3600).to_i.to_s.rjust(2, '0') + ':' + ((sec % 3600) / 60).to_i.to_s.rjust(2, '0') + ':' + ((sec % 3600) % 60).to_i.to_s.rjust(2, '0') if sec
    end   

    def new_leave
			Leave.new
		end

		def period_from
			(params[:from] || Time.now.beginning_of_month).to_date
		end	

		def period_to
			(params[:to] || Time.now).to_date
		end	

		def paydate
			(params[:paydate] || Time.now).to_date
		end	

		def datasummary
			Datasummary.first || Datasummary.new
		end	

		def data_type
			params[:data]=='saved' ? 'from <span style="color:red">Paydate '+ params[:paydate] +'</span>' : 'from <span style="color:green">Timecard</span>'
		end	
	end
end		