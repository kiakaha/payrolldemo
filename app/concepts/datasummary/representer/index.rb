require 'roar/decorator'
require 'roar/json'

class DataRepresenter < Roar::Decorator
  include Roar::JSON

  property :total_days

end  