module Datasummary::Contract
	class Update < Reform::Form
		property :paydate
		property :period_from
		property :period_to
		property :name
  	property :total_days
  	property :reg_ot
  	property :rd_ot
  	property :sp_ot
  	property :hot
  	property :ext_ot
  	property :tardy
  	property :absences
  	property :vl
  	property :sl
  	property :night_diff
  	property :ot_meals
      
  	property :philhealth_15th
  	property :hdmf_15th
  	property :hdmf_ln_15th
  	property :sss_30th
  	property :sss_ln_30th
  	property :insurance
  	property :margot_ca
  	property :adjustments
  	property :pay_13th
  	property :total

		validates :paydate, :period_from, :period_to, presence: true
	end
end			