class Datasummary::Update < Trailblazer::Operation
	step Model( Datasummary, :find )
	step Contract::Build( constant: Datasummary::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end	