class Status::Create < Trailblazer::Operation
	step Model( Status, :new )
	step Contract::Build( constant: Status::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	