class Status::Update < Trailblazer::Operation
	step Model( Status, :find )
	step Contract::Build( constant: Status::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end	