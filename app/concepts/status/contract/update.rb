module Status::Contract
	class Update < Reform::Form
		property :status
		property :description

		validates :status, :description, presence: true   
	end
end		