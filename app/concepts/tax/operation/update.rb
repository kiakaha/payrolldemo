class Tax::Update < Trailblazer::Operation
	step Model( Tax, :find )
	step Contract::Build( constant: Tax::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end	