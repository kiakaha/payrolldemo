class Tax::Create < Trailblazer::Operation
	step Model( Tax, :new )
	step Contract::Build( constant: Tax::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	