module Tax::Contract
	class Create < Reform::Form
		property :exempt_code
		property :over
		property :tax_due
		property :excess_percent
		property :exempt_description

		validates :exempt_code, :over, :tax_due, :excess_percent, :exempt_description, presence: true   
	end
end		