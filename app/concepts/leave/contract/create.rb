module Leave::Contract
	class Create < Reform::Form
  	property :employee_id
  	property :schedule
  	property :kind
  	property :remarks

  	validates :employee_id, :schedule, :kind, :remarks, presence: true 
	end
end		