class Leave::Create < Trailblazer::Operation
	step Model( Leave, :new )
	step Contract::Build( constant: Leave::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	