class Leave::Update < Trailblazer::Operation
	step Model( Leave, :find )
	step Contract::Build( constant: Leave::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	