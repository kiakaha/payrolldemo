module Philhealth::Contract
	class Create < Reform::Form
		property :bracket
  	property :month_salary_from
  	property :month_salary_up_to
  	property :salary_base
  	property :total_monthly_contribution
  	property :personal_share
  	property :employer_share

  	validates :bracket, :month_salary_from, :month_salary_up_to, :salary_base, :total_monthly_contribution, :personal_share, :employer_share, presence: true  
	end
end		