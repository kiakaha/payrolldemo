class Philhealth::Create < Trailblazer::Operation
	step Model( Philhealth, :new )
	step Contract::Build( constant: Philhealth::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	