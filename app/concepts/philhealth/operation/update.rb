class Philhealth::Update < Trailblazer::Operation
	step Model( Philhealth, :find )
	step Contract::Build( constant: Philhealth::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end	