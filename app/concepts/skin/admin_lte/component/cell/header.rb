module Skin::AdminLte::Component::Cell
	class Header < Trailblazer::Cell

		def tasks
			# concept('templates/admin_lte/widget/cell/tasks')
		end

		def messages
			# concept('templates/admin_lte/widget/cell/messages')
		end

		def user_profile

		end

		private

		def user
			model
		end
	end
end