module Skin::AdminLte::Component::Cell
	class Sidebar < Trailblazer::Cell
		
		private

		def active(page)
			if current_page?(page)
				"active"	
			end	
		end 

		def user
			model
		end	
	end
end		