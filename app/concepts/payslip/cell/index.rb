module Payslip::Cell
	class Index < Trailblazer::Cell
		
		private
		
		def employees
			params[:employee] ? Employee.where(:id=>params[:employee]) : params[:branch] ? Employee.where(:branch=>params[:branch]).order(:last_name) : Employee.order(:last_name)
		end	

		def pay(e) 
			s 	     = e.datasummaries.where(:paydate => (params[:paydate]).to_datetime).first 
      tracking = e.trackings.where(:year=>year).last

      if s.present?&&tracking.present?
  			pay	= { 
          :id 							=> s.id,  
          :total_days				=> s.total_days,  
          :reg_ot_time 			=> s.reg_ot, 
          :reg_ot     			=> s.reg_ot.to_f 					* e.hourly_rate.to_f * 1.25, 
          :rd_ot_time      	=> s.rd_ot,
          :rd_ot      			=> s.rd_ot.to_f 					* e.hourly_rate.to_f * 1.3,
          :sp_ot_time  			=> s.sp_ot, 
          :sp_ot      			=> s.sp_ot.to_f 					* e.hourly_rate.to_f * 1.3, 
          :hot_time        	=> s.hot, 
          :hot        			=> s.hot.to_f 						* e.hourly_rate.to_f * 2, 
          :ext_ot_time     	=> s.ext_ot,
          :ext_ot     			=> s.ext_ot.to_f					* e.hourly_rate.to_f * 0.3,
          :tardy      			=> s.tardy.to_f						* (e.hourly_rate.to_f / 60),
          :absences   			=> s.absences.to_f				* e.daily_rate.to_f,
          :vl_used          => s.vl,
          :vl_left    			=> (tracking.vl.to_f || 0) - (s.vl.to_f || 0),
          :vl         			=> s.vl.to_f							* e.daily_rate.to_f,
          :sl_used          => s.sl,
          :sl_left    			=> (tracking.sl.to_f || 0) - (s.sl.to_f || 0),
          :sl         			=> s.sl.to_f							* e.daily_rate.to_f,
          :night_diff 			=> s.night_diff.to_f 			* e.hourly_rate.to_f * 0.1,
          :ot_meals   			=> s.ot_meals.to_f				* e.ot_meal.to_f,

          :philhealth_15th 	=> s.philhealth_15th.to_f * e.government_philhealth.to_f,
          :hdmf_15th   			=> s.hdmf_15th.to_f 			* e.government_hdmf.to_f,
          :hdmf_ln_15th   	=> s.hdmf_ln_15th.to_f 		* e.loan_hdmf.to_f,
          :sss_30th   			=> s.sss_30th.to_f 				* e.government_sss.to_f,
          :sss_ln_30th   		=> s.sss_30th.to_f 				* e.loan_sss.to_f,
          :insurance   			=> s.insurance.to_f 			* e.loan_insurance.to_f,

          :margot_ca   			=> s.margot_ca.to_f 			* e.loan_margot.to_f,
          :adjustments   		=> s.adjustments.to_f,
          :pay_13th   			=> s.pay_13th.to_f,
          :refund   				=> 0,
          :tax_credit				=> 0,
          :other_deductions	=> 0
        } 

        basic               = e.pay_type=='Monthly'||e.pay_type.nil? ? e.basic_salary.to_f / 2 : e.daily_rate.to_f * s.total_days.to_f
     	 	total 							= {
        	:first_total 			=> pay[:reg_ot] + pay[:rd_ot] + pay[:sp_ot] + pay[:hot] + pay[:ext_ot] - pay[:tardy] - pay[:absences] + pay[:vl] + pay[:sl] + pay[:night_diff],
        	:second_total 		=> pay[:philhealth_15th] + pay[:hdmf_15th] + pay[:sss_30th],
        	:ot_total 				=> pay[:reg_ot] + pay[:rd_ot] + pay[:hot] + pay[:sp_ot] + pay[:ext_ot],
        	:earnings_total		=> basic - (pay[:tardy] + pay[:absences]) + pay[:night_diff] + pay[:pay_13th] + (pay[:reg_ot] + pay[:rd_ot] + pay[:hot] + pay[:sp_ot] + pay[:ext_ot]),
        	:other_pays_total => (e.monthly.to_f / 2) + pay[:ot_meals] + pay[:vl] + pay[:sl] + (e.cola.to_f / 2) + pay[:refund] + pay[:tax_credit],
        	:ln_others_total 	=> pay[:sss_ln_30th] + pay[:hdmf_ln_15th] + pay[:margot_ca] + pay[:other_deductions], 
        }

        ot_amount 					= total[:first_total] - total[:second_total]
        ot_tax 							= ot_amount * e.tax_rate.to_f
        tax 								= {
        	:ot_amount 				=> ot_amount,
        	:ot_tax 					=> ot_tax,
        	:net_tax 					=> (e.tax_gross.to_f / 2) + ot_tax  
        }

        paysummary 					= (e.basic_salary.to_f/2) + total[:first_total] - total[:second_total] - pay[:margot_ca] - pay[:adjustments] + pay[:pay_13th] + pay[:refund] - tax[:net_tax] + ((e.monthly.to_f + e.cola.to_f) / 2) + pay[:ot_meals] - pay[:sss_ln_30th] - pay[:hdmf_ln_15th]
        payslip							= 0
        proofing 						= {
        	:paysummary 			=> paysummary,
        	:payslip 					=> payslip,
        	:diff 						=> paysummary - payslip,
        	:gov_deduct_total => pay[:sss_30th] + pay[:philhealth_15th] + pay[:hdmf_15th] + tax[:net_tax] + pay[:adjustments] 
        }
        
        [] << pay << total << tax << proofing  
      end  
	  end  

    def new_leave
			Leave.new
		end

    def year
      params[:paydate].to_datetime.strftime("%Y")||Time.current.year
    end 

		def period_from
			params[:from]
		end	

		def period_to
			params[:to] 
		end	

		def paydate
			params[:paydate]
		end	

    def commas(str)
      str.to_s.reverse.gsub(/(\d{3})/,"\\1,").chomp(",").reverse
    end  
	end
end		