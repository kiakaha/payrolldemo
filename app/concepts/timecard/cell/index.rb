module Timecard::Cell
	class Index < Trailblazer::Cell
		property :timecards	

		private

		def new_timecard
			Timecard.new
		end	

		def employee
			Employee.find(params[:employee_id])
		end	

		def employees
			Employee.all.order(:last_name)
		end	

	end
end		