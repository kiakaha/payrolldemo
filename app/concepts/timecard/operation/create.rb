class Timecard::Create < Trailblazer::Operation
	step :model
	step Contract::Build( constant: Timecard::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()

	private

	def model
		Model( Timecard, :new )
	end	

	def check_time_in(params)
		t = params['morning_in'] | params['morning_in'] | params['morning_in'] | params['morning_in'] | params['morning_in'] | params['morning_in'] | params['morning_in'] 
		Timecard.where('morning_in LIKE :t OR morning_out LIKE :t OR afternoon_in LIKE :t OR afternoon_out LIKE :t OR overtime_in LIKE :t OR overtime_out LIKE :t', t: "%#{t}%")
		
	end	


	# employee shift { 11pm - 8am } 03/20/2017 11pm - 03/21/2017 8am
	# employee shift { 8am - 5pm }  03/20/2017 8am - 03/20/2017 5pm

	# employee login 11pm 

	# 10/13/2016	THU	10:42:24 PM	
	# 10/13/2016  FRI 7:12:41 AM

end	