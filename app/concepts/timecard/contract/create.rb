module Timecard::Contract
	class Create < Reform::Form
		property :morning_in
  	property :morning_out
  	property :afternoon_in
  	property :afternoon_out
  	property :overtime_in
  	property :overtime_out
    property :reason
    property :remark
	end
end		