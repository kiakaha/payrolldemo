class Payrolltype::Create < Trailblazer::Operation
	step Model( Payrolltype, :new )
	step Contract::Build( constant: Payrolltype::Contract::Create )
	step Contract::Validate()
	step Contract::Persist()
end	