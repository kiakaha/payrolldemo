class Payrolltype::Update < Trailblazer::Operation
	step Model( Payrolltype, :find )
	step Contract::Build( constant: Payrolltype::Contract::Update )
	step Contract::Validate()
	step Contract::Persist()
end	