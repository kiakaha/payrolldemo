module Payrolltype::Contract
	class Create < Reform::Form
		property :code
		property :description
		property :days

		validates :code, :description, :days, presence: true   
	end
end		