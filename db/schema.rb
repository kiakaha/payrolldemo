# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170429064736) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "datasummaries", force: :cascade do |t|
    t.datetime "paydate"
    t.datetime "period_from"
    t.datetime "period_to"
    t.string   "employee_no"
    t.string   "name"
    t.string   "total_days",      default: "0", null: false
    t.string   "reg_ot"
    t.string   "rd_ot"
    t.string   "sp_ot"
    t.string   "hot"
    t.string   "ext_ot"
    t.string   "tardy"
    t.string   "absences"
    t.string   "vl"
    t.string   "sl"
    t.string   "ml"
    t.string   "pl"
    t.string   "bl"
    t.string   "el"
    t.string   "night_diff"
    t.string   "ot_meals"
    t.string   "philhealth_15th"
    t.string   "hdmf_15th"
    t.string   "hdmf_ln_15th"
    t.string   "sss_30th"
    t.string   "sss_ln_30th"
    t.string   "insurance"
    t.string   "margot_ca"
    t.string   "adjustments"
    t.string   "pay_13th"
    t.string   "total"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["employee_no"], name: "index_datasummaries_on_employee_no", using: :btree
  end

  create_table "deductions", force: :cascade do |t|
    t.string   "code"
    t.string   "description"
    t.string   "kind"
    t.string   "remark"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "branch"
    t.string   "job_no"
    t.string   "employee_no"
    t.string   "given_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "address"
    t.string   "contact_no"
    t.datetime "birthday"
    t.string   "civil_status"
    t.string   "height"
    t.string   "weight"
    t.string   "religion"
    t.string   "education"
    t.string   "skills"
    t.string   "motto"
    t.string   "designation"
    t.string   "team"
    t.string   "employment_status"
    t.string   "photo"
    t.datetime "date_hired"
    t.string   "basic_salary"
    t.string   "daily_rate"
    t.string   "hourly_rate"
    t.string   "night_diff"
    t.string   "rata"
    t.string   "pera"
    t.string   "addl_comp"
    t.string   "bill_rate_per_day"
    t.string   "pay_type"
    t.string   "shift_pattern_regdays_in"
    t.string   "shift_pattern_regdays_out"
    t.string   "shift_pattern_weekdays_in"
    t.string   "shift_pattern_weekdays_out"
    t.string   "shift_pattern_rd"
    t.string   "monthly"
    t.string   "cola"
    t.string   "ot_meal"
    t.string   "pib"
    t.string   "clothing"
    t.string   "midyear"
    t.string   "loyalty"
    t.string   "year_end"
    t.string   "other_1"
    t.string   "other_2"
    t.string   "other_3"
    t.string   "dependent_1"
    t.string   "dependent_2"
    t.string   "dependent_3"
    t.string   "dependent_4"
    t.string   "dependent_5"
    t.string   "government_sss"
    t.string   "government_hdmf"
    t.string   "government_philhealth"
    t.string   "government_insurance"
    t.string   "tax_exemption"
    t.string   "tax_bracket"
    t.string   "tax_delta"
    t.string   "tax_rate"
    t.string   "tax_add_on"
    t.string   "tax_constant"
    t.string   "tax_gross"
    t.string   "loan_sss"
    t.string   "loan_hdmf"
    t.string   "loan_margot"
    t.string   "loan_insurance"
    t.string   "loan_amount"
    t.string   "date_granted"
    t.string   "loan_interest"
    t.string   "loan_payments"
    t.string   "loan_balance"
    t.string   "bankaccount_no"
    t.string   "sss_no"
    t.string   "philhealth_no"
    t.string   "hdmf_no"
    t.string   "tin_no"
    t.string   "hmo_no"
    t.string   "gsis_no"
    t.string   "atm_no"
    t.string   "bank_name"
    t.string   "bank_branch"
    t.datetime "date_terminated"
    t.string   "reason"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "leaves", force: :cascade do |t|
    t.string   "employee_no"
    t.datetime "schedule"
    t.string   "kind"
    t.string   "remarks"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["employee_no"], name: "index_leaves_on_employee_no", using: :btree
  end

  create_table "leavetypes", force: :cascade do |t|
    t.string   "code"
    t.string   "description"
    t.string   "kind"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "payrolltypes", force: :cascade do |t|
    t.string   "code"
    t.string   "description"
    t.string   "days"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "payslips", force: :cascade do |t|
    t.datetime "paydate"
    t.datetime "period_from"
    t.datetime "period_to"
    t.string   "tardy_absences"
    t.string   "night_diff"
    t.string   "month_13th"
    t.string   "overtime"
    t.string   "sss"
    t.string   "phic"
    t.string   "hmdf"
    t.string   "withholding_tax"
    t.string   "adjustment"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "philhealths", force: :cascade do |t|
    t.string   "bracket"
    t.string   "month_salary_from"
    t.string   "month_salary_up_to"
    t.string   "salary_base"
    t.string   "total_monthly_contribution"
    t.string   "personal_share"
    t.string   "employer_share"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.string   "status"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "taxes", force: :cascade do |t|
    t.string   "exempt_code"
    t.string   "over"
    t.string   "tax_due"
    t.string   "excess_percent"
    t.string   "exempt_description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "timecards", force: :cascade do |t|
    t.string   "employee_no"
    t.datetime "morning_in"
    t.datetime "morning_out"
    t.datetime "afternoon_in"
    t.datetime "afternoon_out"
    t.datetime "overtime_in"
    t.datetime "overtime_out"
    t.string   "reason"
    t.string   "remarks"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["employee_no"], name: "index_timecards_on_employee_no", using: :btree
  end

  create_table "trackings", force: :cascade do |t|
    t.string   "employee_no"
    t.string   "name"
    t.integer  "year"
    t.integer  "sl",          default: 0, null: false
    t.integer  "vl",          default: 0, null: false
    t.integer  "ml",          default: 0, null: false
    t.integer  "pl",          default: 0, null: false
    t.integer  "el",          default: 0, null: false
    t.integer  "bl",          default: 0, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["employee_no"], name: "index_trackings_on_employee_no", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                   default: "", null: false
    t.string   "username",               default: "", null: false
    t.string   "role",                   default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
