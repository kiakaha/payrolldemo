class CreateTrackings < ActiveRecord::Migration[5.0]
  def change
    create_table :trackings do |t|
    	t.string :employee_no
    	t.string :name
      t.integer :year
    	t.integer :sl, default: 0, null: false
    	t.integer :vl, default: 0, null: false
    	t.integer :ml, default: 0, null: false
    	t.integer :pl, default: 0, null: false
    	t.integer :el, default: 0, null: false
    	t.integer :bl, default: 0, null: false
      t.timestamps
    end
    add_index(:trackings, :employee_no)
  end
end
