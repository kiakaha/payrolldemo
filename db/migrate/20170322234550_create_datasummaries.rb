class CreateDatasummaries < ActiveRecord::Migration[5.0]
  def change
    create_table :datasummaries do |t|
    	t.datetime :paydate
    	t.datetime :period_from
    	t.datetime :period_to
    	t.string :employee_no
        t.string :name
    	t.string :total_days, default: 0, null: false
    	t.string :reg_ot
    	t.string :rd_ot
    	t.string :sp_ot
    	t.string :hot
    	t.string :ext_ot
    	t.string :tardy
    	t.string :absences
    	t.string :vl
        t.string :sl
        t.string :ml
        t.string :pl
        t.string :bl
    	t.string :el
    	t.string :night_diff
    	t.string :ot_meals
        
    	t.string :philhealth_15th
    	t.string :hdmf_15th
    	t.string :hdmf_ln_15th
    	t.string :sss_30th
    	t.string :sss_ln_30th
    	t.string :insurance
    	t.string :margot_ca
    	t.string :adjustments
    	t.string :pay_13th
    	t.string :total

      t.timestamps
    end
    add_index(:datasummaries, :employee_no)
  end
end
