class CreateTimecards < ActiveRecord::Migration[5.0]
  def change
    create_table :timecards do |t|
    	t.string :employee_no
    	t.datetime :morning_in
    	t.datetime :morning_out
    	t.datetime :afternoon_in
    	t.datetime :afternoon_out
    	t.datetime :overtime_in
    	t.datetime :overtime_out
      t.string :reason
      t.string :remarks

      t.timestamps
    end
    add_index(:timecards, :employee_no)
  end
end
