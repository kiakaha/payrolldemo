class CreatePayrolltypes < ActiveRecord::Migration[5.0]
  def change
    create_table :payrolltypes do |t|
    	t.string :code
    	t.string :description
    	t.string :days
      t.timestamps
    end
  end
end
