class CreateLeaves < ActiveRecord::Migration[5.0]
  def change
    create_table :leaves do |t|
    	t.string :employee_no
    	t.datetime :schedule
    	t.string :kind
    	t.string :remarks
      t.timestamps
    end
    add_index(:leaves, :employee_no)
  end
end
