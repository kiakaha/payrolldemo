class CreatePayslips < ActiveRecord::Migration[5.0]
  def change
    create_table :payslips do |t|
    	t.datetime :paydate
    	t.datetime :period_from
    	t.datetime :period_to

    	# EARNINGS ==========================
    	t.string :tardy_absences
    	t.string :night_diff
    	t.string :month_13th
    	t.string :overtime

    	# GOVERNMENT DEDUCTIONS
    	t.string :sss
    	t.string :phic
    	t.string :hmdf
    	t.string :withholding_tax
    	t.string :adjustment

      t.timestamps
    end
  end
end
