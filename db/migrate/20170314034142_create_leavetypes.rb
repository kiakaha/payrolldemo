class CreateLeavetypes < ActiveRecord::Migration[5.0]
  def change
    create_table :leavetypes do |t|
    	t.string :code
    	t.string :description
    	t.string :kind
      t.timestamps
    end
  end
end
