class CreateTaxes < ActiveRecord::Migration[5.0]
  def change
    create_table :taxes do |t|
      t.string :exempt_code
    	t.string :over
    	t.string :tax_due
      t.string :excess_percent
      t.string :exempt_description
    	
      t.timestamps
    end
  end
end
