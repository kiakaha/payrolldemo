class CreateDeductions < ActiveRecord::Migration[5.0]
  def change
    create_table :deductions do |t|
    	t.string :code
    	t.string :description
    	t.string :kind
    	t.string :remark

      t.timestamps
    end
  end
end
