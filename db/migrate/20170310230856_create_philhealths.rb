class CreatePhilhealths < ActiveRecord::Migration[5.0]
  def change
    create_table :philhealths do |t|
    	t.string :bracket
    	t.string :month_salary_from
    	t.string :month_salary_up_to
    	t.string :salary_base
    	t.string :total_monthly_contribution
    	t.string :personal_share
    	t.string :employer_share
      t.timestamps
    end
  end
end
