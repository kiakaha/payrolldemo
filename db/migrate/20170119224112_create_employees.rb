class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
    	# PERSONAL INFORMATION ================================
			t.string :branch
			t.string :job_no
			t.string :employee_no
			t.string :given_name
			t.string :middle_name
			t.string :last_name
			t.string :email
			t.string :address
			t.string :contact_no
			t.datetime :birthday
			t.string :civil_status
			t.string :height
			t.string :weight
			t.string :religion
			t.string :education
			t.string :skills
			t.string :motto
			t.string :designation
			t.string :team
			t.string :employment_status
			t.string :photo
			t.datetime :date_hired
			# =====================================================
			
			# BASIC PAY ===========================================
			t.string :basic_salary
			t.string :daily_rate # basic_salary / 26
			t.string :hourly_rate # daily_rate / 8
			t.string :night_diff 

			t.string :rata
			t.string :pera
			t.string :addl_comp
			
			t.string :bill_rate_per_day
			t.string :pay_type
			t.string :shift_pattern_regdays_in
			t.string :shift_pattern_regdays_out
			t.string :shift_pattern_weekdays_in # WEEKEND IN
			t.string :shift_pattern_weekdays_out  # WEEKEND OUT
			t.string :shift_pattern_rd
			# =====================================================

			# OTHER ALLOWANCE/BONUS ===============================
			t.string :monthly
			t.string :cola
			t.string :ot_meal

			t.string :pib
			t.string :clothing
			t.string :midyear
			t.string :loyalty
			t.string :year_end
			t.string :other_1
			t.string :other_2
			t.string :other_3
			t.string :dependent_1	
			t.string :dependent_2
			t.string :dependent_3
			t.string :dependent_4
			t.string :dependent_5
			# =====================================================

			# GOVERNMENT ==========================================
			t.string :government_sss
			t.string :government_hdmf
			t.string :government_philhealth
			t.string :government_insurance
			# =====================================================

			# TAXATION ============================================
			t.string :tax_exemption 
			t.string :tax_bracket
			t.string :tax_delta
			t.string :tax_rate
			t.string :tax_add_on
			t.string :tax_constant
			t.string :tax_gross
			# delta = basic_salary - tax_not_over
			# rate = tax_excess_percent
			# add_on = delta * rate
			# constant = tax_due
			# gross_tax = add-on + constant
					# --------- FROM PAY SUMMARY ---------------------------------------------
					# additional = REG_OT + RDOT + SP_OT + EXT_OT - TARDY - ABSENCES + VL + SL 
					# deduction  = PHILHEALTH(15th) + HDMF(15th) + SSS(30th)
			# ot_amount = additional - deduction
			# ot_tax = ot_amount * rate
			# net_tax = gross_tax + ot_tax
			# ================================================================

			# LOANS ==========================================================
			t.string :loan_sss
			t.string :loan_hdmf
			t.string :loan_margot
			t.string :loan_insurance

			t.string :loan_amount
			t.string :date_granted
			t.string :loan_interest
			t.string :loan_payments
			t.string :loan_balance

			# ================================================================

			# IMPORTANT NUMBERS ==============================================
			t.string :bankaccount_no
			t.string :sss_no
			t.string :philhealth_no
			t.string :hdmf_no
			t.string :tin_no
			t.string :hmo_no

			t.string :gsis_no
			t.string :atm_no
			t.string :bank_name
			t.string :bank_branch
			# ================================================================

			# @remove t.string :no_of_children			

			# TERMINATION ====================================================
			t.datetime :date_terminated
			t.string :reason
			# ================================================================

    	t.timestamps
    end
  end
end
