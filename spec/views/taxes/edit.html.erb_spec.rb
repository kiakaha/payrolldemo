require 'rails_helper'

RSpec.describe "taxes/edit", type: :view do
  before(:each) do
    @tax = assign(:tax, Tax.create!())
  end

  it "renders the edit tax form" do
    render

    assert_select "form[action=?][method=?]", tax_path(@tax), "post" do
    end
  end
end
