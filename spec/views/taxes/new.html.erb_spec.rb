require 'rails_helper'

RSpec.describe "taxes/new", type: :view do
  before(:each) do
    assign(:tax, Tax.new())
  end

  it "renders new tax form" do
    render

    assert_select "form[action=?][method=?]", taxes_path, "post" do
    end
  end
end
