require 'rails_helper'

RSpec.describe "datasummaries/new", type: :view do
  before(:each) do
    assign(:datasummary, Datasummary.new())
  end

  it "renders new datasummary form" do
    render

    assert_select "form[action=?][method=?]", datasummaries_path, "post" do
    end
  end
end
