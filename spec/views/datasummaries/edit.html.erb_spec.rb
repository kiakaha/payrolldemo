require 'rails_helper'

RSpec.describe "datasummaries/edit", type: :view do
  before(:each) do
    @datasummary = assign(:datasummary, Datasummary.create!())
  end

  it "renders the edit datasummary form" do
    render

    assert_select "form[action=?][method=?]", datasummary_path(@datasummary), "post" do
    end
  end
end
