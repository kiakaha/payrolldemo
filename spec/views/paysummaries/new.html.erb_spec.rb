require 'rails_helper'

RSpec.describe "paysummaries/new", type: :view do
  before(:each) do
    assign(:paysummary, Paysummary.new())
  end

  it "renders new paysummary form" do
    render

    assert_select "form[action=?][method=?]", paysummaries_path, "post" do
    end
  end
end
