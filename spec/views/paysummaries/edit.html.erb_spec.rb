require 'rails_helper'

RSpec.describe "paysummaries/edit", type: :view do
  before(:each) do
    @paysummary = assign(:paysummary, Paysummary.create!())
  end

  it "renders the edit paysummary form" do
    render

    assert_select "form[action=?][method=?]", paysummary_path(@paysummary), "post" do
    end
  end
end
