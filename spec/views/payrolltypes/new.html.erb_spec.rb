require 'rails_helper'

RSpec.describe "payrolltypes/new", type: :view do
  before(:each) do
    assign(:payrolltype, Payrolltype.new())
  end

  it "renders new payrolltype form" do
    render

    assert_select "form[action=?][method=?]", payrolltypes_path, "post" do
    end
  end
end
