require 'rails_helper'

RSpec.describe "payrolltypes/edit", type: :view do
  before(:each) do
    @payrolltype = assign(:payrolltype, Payrolltype.create!())
  end

  it "renders the edit payrolltype form" do
    render

    assert_select "form[action=?][method=?]", payrolltype_path(@payrolltype), "post" do
    end
  end
end
