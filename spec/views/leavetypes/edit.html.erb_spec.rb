require 'rails_helper'

RSpec.describe "leavetypes/edit", type: :view do
  before(:each) do
    @leavetype = assign(:leavetype, Leavetype.create!())
  end

  it "renders the edit leavetype form" do
    render

    assert_select "form[action=?][method=?]", leavetype_path(@leavetype), "post" do
    end
  end
end
