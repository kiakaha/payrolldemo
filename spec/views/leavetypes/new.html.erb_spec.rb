require 'rails_helper'

RSpec.describe "leavetypes/new", type: :view do
  before(:each) do
    assign(:leavetype, Leavetype.new())
  end

  it "renders new leavetype form" do
    render

    assert_select "form[action=?][method=?]", leavetypes_path, "post" do
    end
  end
end
