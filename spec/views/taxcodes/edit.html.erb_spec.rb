require 'rails_helper'

RSpec.describe "taxcodes/edit", type: :view do
  before(:each) do
    @taxcode = assign(:taxcode, Taxcode.create!())
  end

  it "renders the edit taxcode form" do
    render

    assert_select "form[action=?][method=?]", taxcode_path(@taxcode), "post" do
    end
  end
end
