require 'rails_helper'

RSpec.describe "taxcodes/new", type: :view do
  before(:each) do
    assign(:taxcode, Taxcode.new())
  end

  it "renders new taxcode form" do
    render

    assert_select "form[action=?][method=?]", taxcodes_path, "post" do
    end
  end
end
