require 'rails_helper'

RSpec.describe "deductions/new", type: :view do
  before(:each) do
    assign(:deduction, Deduction.new())
  end

  it "renders new deduction form" do
    render

    assert_select "form[action=?][method=?]", deductions_path, "post" do
    end
  end
end
