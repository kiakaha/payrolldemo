require 'rails_helper'

RSpec.describe "deductions/edit", type: :view do
  before(:each) do
    @deduction = assign(:deduction, Deduction.create!())
  end

  it "renders the edit deduction form" do
    render

    assert_select "form[action=?][method=?]", deduction_path(@deduction), "post" do
    end
  end
end
