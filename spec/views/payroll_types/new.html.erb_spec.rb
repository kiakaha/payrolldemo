require 'rails_helper'

RSpec.describe "payroll_types/new", type: :view do
  before(:each) do
    assign(:payroll_type, PayrollType.new())
  end

  it "renders new payroll_type form" do
    render

    assert_select "form[action=?][method=?]", payroll_types_path, "post" do
    end
  end
end
