require 'rails_helper'

RSpec.describe "payroll_types/edit", type: :view do
  before(:each) do
    @payroll_type = assign(:payroll_type, PayrollType.create!())
  end

  it "renders the edit payroll_type form" do
    render

    assert_select "form[action=?][method=?]", payroll_type_path(@payroll_type), "post" do
    end
  end
end
