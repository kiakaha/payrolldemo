require "rails_helper"

RSpec.describe DatasummariesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/datasummaries").to route_to("datasummaries#index")
    end

    it "routes to #new" do
      expect(:get => "/datasummaries/new").to route_to("datasummaries#new")
    end

    it "routes to #show" do
      expect(:get => "/datasummaries/1").to route_to("datasummaries#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/datasummaries/1/edit").to route_to("datasummaries#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/datasummaries").to route_to("datasummaries#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/datasummaries/1").to route_to("datasummaries#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/datasummaries/1").to route_to("datasummaries#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/datasummaries/1").to route_to("datasummaries#destroy", :id => "1")
    end

  end
end
