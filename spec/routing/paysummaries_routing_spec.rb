require "rails_helper"

RSpec.describe PaysummariesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/paysummaries").to route_to("paysummaries#index")
    end

    it "routes to #new" do
      expect(:get => "/paysummaries/new").to route_to("paysummaries#new")
    end

    it "routes to #show" do
      expect(:get => "/paysummaries/1").to route_to("paysummaries#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/paysummaries/1/edit").to route_to("paysummaries#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/paysummaries").to route_to("paysummaries#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/paysummaries/1").to route_to("paysummaries#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/paysummaries/1").to route_to("paysummaries#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/paysummaries/1").to route_to("paysummaries#destroy", :id => "1")
    end

  end
end
