require "rails_helper"

RSpec.describe PayrollTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/payroll_types").to route_to("payroll_types#index")
    end

    it "routes to #new" do
      expect(:get => "/payroll_types/new").to route_to("payroll_types#new")
    end

    it "routes to #show" do
      expect(:get => "/payroll_types/1").to route_to("payroll_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/payroll_types/1/edit").to route_to("payroll_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/payroll_types").to route_to("payroll_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/payroll_types/1").to route_to("payroll_types#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/payroll_types/1").to route_to("payroll_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/payroll_types/1").to route_to("payroll_types#destroy", :id => "1")
    end

  end
end
