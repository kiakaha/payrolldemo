require "rails_helper"

RSpec.describe PayrolltypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/payrolltypes").to route_to("payrolltypes#index")
    end

    it "routes to #new" do
      expect(:get => "/payrolltypes/new").to route_to("payrolltypes#new")
    end

    it "routes to #show" do
      expect(:get => "/payrolltypes/1").to route_to("payrolltypes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/payrolltypes/1/edit").to route_to("payrolltypes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/payrolltypes").to route_to("payrolltypes#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/payrolltypes/1").to route_to("payrolltypes#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/payrolltypes/1").to route_to("payrolltypes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/payrolltypes/1").to route_to("payrolltypes#destroy", :id => "1")
    end

  end
end
