require "rails_helper"

RSpec.describe TaxcodesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/taxcodes").to route_to("taxcodes#index")
    end

    it "routes to #new" do
      expect(:get => "/taxcodes/new").to route_to("taxcodes#new")
    end

    it "routes to #show" do
      expect(:get => "/taxcodes/1").to route_to("taxcodes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/taxcodes/1/edit").to route_to("taxcodes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/taxcodes").to route_to("taxcodes#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/taxcodes/1").to route_to("taxcodes#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/taxcodes/1").to route_to("taxcodes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/taxcodes/1").to route_to("taxcodes#destroy", :id => "1")
    end

  end
end
