Rails.application.routes.draw do
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "employees#index"

  devise_for :users, skip: [:sessions, :registrations, :passwords] 
  as :user do
    get 'register', to: 'users/registrations#new', as: :new_user_registration
    get 'users/edit', to: 'users/registrations#edit', as: :edit_user_registration
    post 'register', to: 'users/registrations#create', as: :user_registration
    get 'login', to: 'users/sessions#new', as: :new_user_session
    post 'login', to: 'users/sessions#create', as: :user_session
    match 'logout', to: 'users/sessions#destroy', as: :destroy_user_session, via: Devise.mappings[:user].sign_out_via
  end

  resources :leaves
  resources :paysummaries do
    collection { get :report }
    collection { get :bankreport }
  end
  resources :datasummaries do
    collection { post :import }
    collection { get :remove }
  end
  resources :payslips do
    collection { get :email }
  end  
  # resources :payrolltypes
  # resources :leavetypes
  # resources :dashboards
  # resources :taxcodes
  # resources :payroll_types
  # resources :statuses
  # resources :philhealths
  # resources :taxes
  # resources :deductions
  resources :trackings do 
    collection { post :import }
  end  
  
  resources :employees do
    resources :timecards do
      collection { post :import }
      collection { post :logs }
      collection { post :summary }
    end

    collection { post :import }
  end
  get 'dashboard', to: 'dashboard#index'
  get 'settings', to: 'settings#index'

  namespace :api do 
    namespace :v1 do 
      resources :employees, only: [:index] 
    end 
  end
end
